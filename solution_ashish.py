import os

inputFilename = "person_wine_3.txt"
textfile = open(inputFilename, 'r')
winelist = {}
personlist = {}
wineInterestPersonList = {}
personsFinalList = {}

'''
/* reading the input file as lines and creating hashes of wines persons and Intrestedwines
'''
for line in textfile:
	(person,wine) = line.split()
	person = person.replace("person","")
	wine = wine.replace("wine","")
	if wine in winelist:
		winelist[wine] = winelist[wine]+1
	else:
		winelist[wine] = 1
	
	if person in personlist:
		personlist[person].append(wine)
	else:
		personlist[person] = [wine]

	if wine in wineInterestPersonList:
		wineInterestPersonList[wine].append(person)
	else:
		wineInterestPersonList[wine] = [person]
textfile.close()

'''
/*if condition
/* unique wine count and assign it to the person
/* update the final list
/* remove the wine bottle from wine list
/* reduce the count
'''

'''
/* else condtion
/* condition to check when the same bottle is desired by more then 1 person
/* checking if he has any 3 unique bottles
/* checking for the final list
/* assigning the bottles accordingly
'''

def assignWine():
	update = True
	while update:
		update = False
		for wine in list(winelist):
			if winelist[wine] == 1:
				person = wineInterestPersonList[wine][0]
				if person in personlist:
					if person in personsFinalList:
						personsFinalList[person].append(wine)
					else:
						personsFinalList[person] = [wine]
					personlist[person].remove(wine)
					if len(personsFinalList[person]) == 3:
						update = True
						for update_wine in personlist[person]:
							winelist[update_wine] = winelist[update_wine] - 1
							wineInterestPersonList[update_wine].remove(person)
						personlist.pop(person)
				winelist.pop(wine)
			else:

				wIntrest = wineInterestPersonList[wine]
				for prsn in wIntrest:
					unique_wine = []

					if prsn in personsFinalList and len(personsFinalList[prsn]) == 3:
						continue

					if prsn in personsFinalList:
						unique_wine.append(len(personsFinalList[prsn]))

					try:
						for nWine in personlist[prsn]:
							if winelist[nWine] == 1:
								unique_wine.append(1)
						if sum(unique_wine)<3 and winelist[nWine] != 1 and winelist[nWine] > 0:
							if prsn in personsFinalList:
								personsFinalList[prsn].append(nWine)
							else:
								personsFinalList[prsn] = [nWine]
							personlist[prsn].remove(nWine)
							winelist[nWine] = 0
						else:
							continue
					except:
						print("error occured")


'''
/* reading the personsFinalList and writing in a file
'''

def outputResult():
	input_file_tagname = os.path.splitext(os.path.basename(inputFilename))[0]
	output_filename = input_file_tagname+"_output.txt"
	textfile = open(output_filename, 'w')
	for person in personsFinalList:
		for winenumber in personsFinalList[person]:
			textfile.write("person"+person+"\t"+"wine"+winenumber+"\n")
	textfile.close()

assignWine()
outputResult()
