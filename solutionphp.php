<?php
// setting up memory allocation
ini_set('memory_limit', '1024M'); 


class WineYard{
	
	public $fileName;

	function __construct($text){
		$this->fileName = $text;
	}

/* Assignning default values IO operation on the file
  Backtracking to check if wine is already assigned */

	public function assignWine(){
		$wineWishlist	= [];
		$wineList 		= [];
		$wineSold 		= 0;
		$finalList 		= [];
		$file 			= fopen($this->fileName,"r");
		while (($line = fgets($file)) !== false) {
			$person_and_wine = explode("\t", $line);
			$name = trim($person_and_wine[0]);
			$wine = trim($person_and_wine[1]);
			if(!array_key_exists($wine, $wineWishlist)){
				$wineWishlist[$wine] = [];
			}
			$wineWishlist[$wine][] = $name;
			$wineList[] = $wine;
		}
		fclose($file);

		$wineList = array_unique($wineList);
		
		/* limiting the unique bottles to be sold for 3
	creating a final assigning list*/

	foreach ($wineList as $key => $wine) {
		$maxSize = count($wine);
		$counter = 0;

		$i = intval(floatval(rand()/(float)getrandmax()) * $maxSize);
		$person = $wineWishlist[$wine][$i];
		if(!array_key_exists($person, $finalList)){
			$finalList[$person] = [];
		}
		if(count($finalList[$person])<3){
			$finalList[$person][] = $wine;
			$wineSold++;
			break;
		}
		$counter++;
	}

// IO for the assigning the bottles

		$fh = fopen("finalAssignPHP.txt", "w");
		fwrite($fh, "Total number of wine bottles sold : ".$wineSold."\n");
		foreach (array_keys($finalList) as $key => $person) {
			foreach ($finalList[$person] as $key => $wine) {
				fwrite($fh, $person." ".$wine."\n");
			}
		}
		fclose($fh);
	}
}
$winePuzzle = new WineYard("person_wine_3.txt");
$winePuzzle->assignWine();
?>
